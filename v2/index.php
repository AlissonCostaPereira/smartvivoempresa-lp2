
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>

  <title>Smart Vivo Empresas</title>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K4PDQS3');</script>
<!-- End Google Tag Manager -->

    <!-- Basic Page Needs
      ================================================== -->
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <meta name="author" content="">
    <!-- Mobile Specific Metas
      ================================================== -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
      <meta http-equiv="x-ua-compatible" content="IE=9">
      <!-- Font Awesome -->
      <link href="stylesheets/font-awesome.css" rel="stylesheet">
      <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    <!-- CSS
      ================================================== -->
      <link rel="stylesheet" href="stylesheets/menu.css">
      <!-- <link rel="stylesheet" href="stylesheets/flat-ui-slider.css"> -->
      <link rel="stylesheet" href="stylesheets/base.css">
      <link rel="stylesheet" href="stylesheets/skeleton.css">
      <link rel="stylesheet" href="stylesheets/landings.css">
      <link rel="stylesheet" href="stylesheets/main.css">
      <link rel="stylesheet" href="stylesheets/landings_layouts.css">
      <link rel="stylesheet" href="stylesheets/box.css">
      <link rel="stylesheet" href="stylesheets/pixicon.css">
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <link href="assets/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
      <link rel="stylesheet" type="text/css" href="stylesheets/owl.carousel.min.css">
      <link rel="stylesheet" type="text/css" href="stylesheets/owl.theme.default.min.css">

    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Favicons
      ================================================== -->
      <link rel="shortcut icon" href="images/faviconn.ico">
      <link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />
      <link rel="stylesheet" href="stylesheets/custom/index.css">
      <link rel="stylesheet" href="stylesheets/custom/teste.css">
    </head>
    <body>

      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4PDQS3"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="page" class="page">
         <div class="pixfort_normal_1 pix_builder_bg" id="section_header_3_dark">
          <div class="header_style header_nav_1 dark">
            <div class="container content-small">
              <div class="sixteen columns firas2">
                <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                  <!-- <div class="containerss"> -->
                    <div class="navbar-header">
                      <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                      </button>
                      <img src="images/uploads/logo-vivo.png" class="logo vivo" alt="">
                      <img src="images/uploads/Smart_logo_logo.png" class="logo smart" alt="">                
                    </div>
                    <div id="navbar-collapse-02" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="active propClone">
                          <a href="#ofertas">Ofertas</a>
                        </li>
                        <li class="propClone">
                          <a href="#contato">Contato</a>
                        </li>
                        <li class="propClone">
                          <a class="" href="#ofertas">
                            <span class="pix_header_button" rel="">         Compre Agora
                            </span>
                          </a>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                    <!-- </div> -->
                  </nav>
                </div>
              </div><!-- container -->
            </div>
          </div>
          <div class="container-fluid" id="banner-mobile">
            <div class="row">
              <div class="col-md">
                <div class="banner-mobile">    
                </div>
              </div>
            </div>
          </div>
          <div class="pixfort_gym_13 construction " id="section_gym_5">
           <div class="join_us_section pix_builder_bg">
            <div class="container content-small">
             <div class="row">
               <div class="col-md-12">
                <div class="pix_form_area">
                  <div class="substyle pix_builder_bg">
                   <div class="title-style">
                    Ligue agora<br/>
                    <p class="number-tel">
                      0800 608 1128
                    </p>
                    <p class="text-ligamos">
                      ou nós ligamos para você!
                    </p>
                  </div>
                  <div class="clearfix"></div>
                  <form id="contact_form" class="form_lead" method="get" action="dados.php">
                    <input type="text" name="nome" id="name" placeholder="Nome" class="pix_text" required="" >
                    <input type="email" name="email" id="email" placeholder="E-mail" class="pix_text" required="" >
                    <input type="text" name="telefone" id="number" placeholder="Telefone" class="pix_text" required="" >
                    <span class="send_btn">
                     <button id="submit_btn_131" class="slow_fade pix_text" onclick="EnviaDados();" type="button">
                      Enviar
                    </button>
                  </span>
                </form>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="hidden_pix_13" class="confirm_page confirm_page_13 construction pix_builder_bg">
   <div class="pixfort_gym_13 ">
    <div class="confirm_header">
     <span class="editContent">
      <span class="pix_text">
       Obrigado por enviar sua mensagem!
     </span>
   </span>
 </div>
 <div class="confirm_text">
   <span class="editContent">
    <span class="pix_text">
     Sua mensagem foi enviada com sucesso!
   </span>
 </span>
</div>
<div class="center_text padding_bottom_60">

</div>
</div>
</div>
<section class="ofertas-session" id="ofertas">
  <div class="container">   
    <div class="row">
      <div class="col-md-12">
        <h2 class="title-ofertas">
          Confira mais ofertas de Local Empresas e Brasil Empresas
        </h2>
      </div>
    </div>
    <div class="row">   
      <div class="col-md-12">
        <div class="owl-carousel owl-theme">
          <div class="item">
            <?php require_once("include_25.php"); ?>
            <?php require_once("include_50.php"); ?>
            <?php require_once("include_100.php"); ?>
          </div>
          <div class="item">
            <?php require_once("brasil-empresa_25.php"); ?>
            <?php require_once("brasil-empresa_50.php"); ?>
            <?php require_once("brasil-empresa_100.php"); ?>
          </div>
        </div>




        <div class="modal fade form-modal" id="modal-mensagem">
          <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
               <h4 class="modal-title">Título da mensagem</h4>
             </div>
             <div class="modal-body">

              <div class="title-modal-number">
                Ligue agora<br/>
                <p class="number-tel">
                  0800 608 1128
                </p>
                <p class="text-ligamos">
                  ou nós ligamos para você!
                </p>
              </div>

              <form id="contact_form2" class="form-modal" method="get" action="dados-news.php">
                <div id="result">                              
                </div>
                <input type="text" name="nome" id="name" placeholder="Nome" class="pix_text-modal" required="" >

                <input type="email" name="email" id="email" placeholder="E-mail" class="pix_text-modal" required="" >

                <input type="text" name="telefone" id="number" placeholder="Telefone" class="pix_text-modal" required="">
                <span class="send_btn">
                 <button id="submit_btn_13" class="slow_fade button-modal" onclick="EnviaDadosModal();">
                  Enviar
                </button>
              </span>
            </form>

          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-default fechar-modal" data-dismiss="modal">Fechar</button>
         </div>
       </div>
     </div>
   </div>

 </div>
</div>  
</div>
</section>

<div class="pixfort_subscribe_12" id="contato">
  <div class="page_style pix_builder_bg" style="outline-offset: -3px;">
   <div class="container content-small">
    <div class="sixteen columns">
     <div class="context_style pix_builder_bg">
      <div class="title_style oficcina-bold">
       <span class="editContent">Inscreva-se e receba mais informações</span>
     </div>
     <div class="subtitle_style">
       <span class="editContent" style=""></span>
     </div>
     <div class="email_subscribe  subscribe_btn">
       <form id="a" class="form-subs formulario" action="dados-news.php">
        <div id="result"></div>
        <input name="email" id="email" placeholder="E-mail" class="pix_text input-subs" type="email" required="" style="padding: 9px;display: inline-block;border: solid 1px;">

        <button class="subscribe_btn pix_text button-subs" id="subscribe_btn_12" onclick="EnviaDadosNews();">
         <span class="editContent" style="">Enviar</span>
       </button>
     </form>
     <div class="clearfix"></div>
     <div class="note_st"><span class="editContent" style=""></span></div>
   </div>
 </div>
</div>
</div>
</div>
</div>
<div id="hidden_pix_12" class="confirm_page confirm_page_12 pix_builder_bg" style="display: none;">
  <div class="pixfort_subscribe_12">
   <div class="confirm_logo"><span class="conf_img"><img src="images/12_subscribe/confirm1.png"></span></div>
   <div class="confirm_header"><span class="editContent" style=""><span class="pix_text">Obrigado por se inscrever!</span></span></div>
   <div class="confirm_text">
    <span class="editContent"><span class="pix_text">Aproveite nossos serviços.</span></span>
  </div>

  <div class="center_text padding_bottom_60">
    <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
      <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
      <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
      <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
    </ul>
  </div>
</div>
</div>
<footer>
  <div class="container" align="center">
    <div class="row">
      <div class="col-md-6">
        <figure>
          <img src="images/uploads/Smart_logo_logo.png" class="logo smart" alt="">
        </figure>
      </div>
      <div class="col-md-6">
        <div class="contatos">
          <b>
            0800 608 1128
          </b><br/>
          <p class="endereco">
            Marcondes Pereira, 625,<br/> Dionísio Torres. Fortaleza - CE
          </p>
        </div>
      </div>
    </div>
    <p class="direitos">
      © Todos os direitos reservados.
    </p>
  </div>
</footer>

<!-- JavaScript
  ================================================== -->
  <script src="js-files/jquery-1.8.3.min.js" type="text/javascript"></script>
  <script src="js-files/jquery.easing.1.3.js" type="text/javascript"></script>
  <script type="text/javascript" src="js-files/jquery.common.min.js"></script>
  <script src="js-files/ticker.js" type="text/javascript"></script>
  <!-- <script src="js-files/custom1.js" type="text/javascript"></script> -->
  <script src="assets/js/smoothscroll.min.js" type="text/javascript"></script>
  <script src="assets/js/appear.min.js" type="text/javascript"></script>
  <script src="js-files/jquery.ui.touch-punch.min.js"></script>
  <script src="js-files/bootstrap.min.js"></script>
  <script src="js-files/bootstrap-switch.js"></script>
  <!-- <script src="js-files/custom3.js" type="text/javascript"></script> -->
  <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
  <script src="assets/js/appear.min.js" type="text/javascript"></script>
  <script src="assets/js/animations.js" type="text/javascript"></script>
  <script src="teste.js" type="text/javascript"></script>
</body>
</html>