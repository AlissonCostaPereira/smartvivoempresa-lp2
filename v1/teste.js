// $(".btn-toggle").on('click', function(){

// 	meuElemento = $(this).parent().find(".card-content .wrap-infos-card");
// 	meuElementoIndice = $(".card-content .wrap-infos-card").index(meuElemento);
// 	$(".card-content .wrap-infos-card").each(function(index){
// 		if (index != meuElementoIndice) {
// 			$(".card-content .wrap-infos-card").eq(index).slideUp();
// 		}
// 	});
// 	$(this).toggleClass('active');
// 	$(".btn-toggle").parent().find(".card-content .wrap-infos-card").slideToggle();
// });

$('.owl-carousel').owlCarousel({
    autoplay:true,
    loop:true,
    margin:10,
    nav: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        767:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});


$('#btn-mensagem').on('shown.bs.modal', function () {
  $('#modal-mensagem').focus()
});
function validar_mobile(){
    var Largura = window.innerWidth;
    if (Largura < 1000) {
        $(".content-small").removeClass('container');
        $(".content-small").addClass('container-fluid');
    } else{
        $(".content-small").addClass('container');
        $(".content-small").removeClass('container-fluid');
    }
}
$(window).resize(function(){
    validar_mobile();
});

$(document).ready(function(){
    validar_mobile();
});

