<div class="col-md-4">
	<article class="cards-combos header-card-purple   double flag-top " ng-show="card.show" ng-if="!$root.isMobile || componente.withoutCarousel" ng-repeat="card in componente.dataCards.slice($index, $index + 3)" ng-init="$root.cards = componente.dataCards" card="card" horizontal="componente.horizontalCard" aria-hidden="false">
	<header class="header-card">
		<!-- ngIf: card.labelOffer -->
		<span ng-if="card.labelOffer" class="label-offer top background-green" ng-bind-html="card.labelOffer">
		Oferta Especial</span>
		<!-- end ngIf: card.labelOffer -->
		<h2 class="title-header-card ">
			<strong ng-bind="card.title">
			Combos 2 em 1</strong>
			<!-- ngIf: card.title -->
			<small ng-if="card.title" ng-bind-html="card.subtitle" class="">
			</small>
			<!-- end ngIf: card.title -->
		</h2>
		<!-- ngIf: !card.hideButtonHeader -->
	
		<!-- end ngIf: !card.hideButtonHeader -->
	</header>
	<div ng-init="card.showDetail = false" class="content-card-full ">
		<!-- ngRepeat: valor in arrDataComb -->
		<!-- ngIf: valor.chave != 'adicionais' -->
		<div class="wrap-content-card box-banda" ng-class="{'last-element':$last, 'prod-offer':valor.offer || valor.offerExclusive }" ng-repeat="valor in arrDataComb " ng-if="valor.chave != 'adicionais'" chave="banda_100mb">
			<!-- ngIf: valor.offerExclusive  && !$root.isMobile -->
			<div class="wrap-small" mk-comp="atom-icon-card" chave="banda">
				<span>
					<!-- ngIf: componente.icon -->
					<svg class="ico-card" svg-id="icon-wifi" ng-if="componente.icon">
						<use xlink:href="assets/icon/icon.svg#icon-wifi">
						</use>
					</svg>
					<!-- end ngIf: componente.icon -->
					<h3 class="title-wrap-small" ng-bind-html="componente.name">
					Internet</h3>
				</span>
			</div>
			<div class="wrap-content-medium">
				<!-- ngIf: valor.offer && !$root.isMobile -->
				<div class="card-content banda" ng-class="{centralize:!card.showDetail}">
					<div class="wrap-info-card" mk-comp="molecule-simple-header-card" chave="banda_100mb" parametros="{quantity: valor.quantity}">
						<!-- ngIf: componente.name -->
						<!-- ngIf: componente.canais -->
						<!-- ngIf: componente.velocidade -->
						<h4 class="plan-last-name" ng-if="componente.velocidade">
							<strong>
							100 Mega</strong>
						</h4>
						<!-- end ngIf: componente.velocidade -->
						<!-- ngIf: componente.descriptionOld -->
						<!-- ngIf: componente.description -->
						<!-- ngIf: componente.dependents -->
						<!-- ngIf: componente.wifi -->
						<span class="channel-hd" ng-if="componente.wifi">
							<strong>
							Wi-fi</strong>
							<em>
							Grátis</em>
						</span>
						<!-- end ngIf: componente.wifi -->
						<div class="info-4p ng-hide" ng-show="componente.minutes || componente.movel_dependents" aria-hidden="true">
							<!-- ngIf: componente.minutesOld -->
							<span ng-bind-html="componente.minutes">
							</span>
							<span ng-bind-html="componente.movel_dependents">
							</span>
						</div>
						<!-- ngIf: componente.canaisHD -->
						<!-- ngIf: parametros.quantity -->
					</div>
					<!-- ngIf: valor.chave == 'tv' -->
					<!-- ngRepeat: adicional in valor.adicionais -->
					<!-- ngIf: (valor.offer || valor.offerExclusive) -->
					<div class="wrap-infos-card  show-itens" mk-comp="molecule-infos-card" chave="banda_100mb" parametros="{sameHeight: false,hideTitle:true, cardType:'single-plans', show: card.showDetail, mkt_id: valor.mkt_id}">
						<!-- ngIf: componente.title && !parametros.hideTitle -->
						<!-- ngRepeat: (key, valor) in  componente[parametros.opcoes.selecionado] -->
						<!-- ngIf: componente.list && parametros.show -->
						<ul class="list list-check b2b color-gray-base-3 subtitle-5 border-bottom" list="componente.list" list-style="componente.listStyle" list-type="parametros.cardType" ng-if="componente.list &amp;&amp; parametros.show" parametros="{cardType:componente.list.listType}">
							<!-- ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							Download: 100 Mbps</li>
							<!-- end ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							Upload: 50 Mbps</li>
							<!-- end ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							Instalação &amp; Configuração do Modem</li>
							<!-- end ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							1 TB de espaço no Cloud Backup</li>
							<!-- end ngRepeat: item in list track by $index -->
						</ul>
						<!-- end ngIf: componente.list && parametros.show -->
						<!-- ngIf: componente.popover -->
						<!-- ngIf: parametros.card.labelOffer && !$root.isMobile -->
					</div>
				</div>
			</div>
		</div>
		<!-- end ngIf: valor.chave != 'adicionais' -->
		<!-- end ngRepeat: valor in arrDataComb -->
		<!-- ngIf: valor.chave != 'adicionais' -->
		<div class="wrap-content-card box-telefonia last-element" ng-class="{'last-element':$last, 'prod-offer':valor.offer || valor.offerExclusive }" ng-repeat="valor in arrDataComb " ng-if="valor.chave != 'adicionais'" chave="telefonia_fixo_movel_ilimitado_local_empresas">
			<!-- ngIf: valor.offerExclusive  && !$root.isMobile -->
			<div class="wrap-small" mk-comp="atom-icon-card" chave="telefonia">
				<span>
					<!-- ngIf: componente.icon -->
					<svg class="ico-card" svg-id="icon-phone" ng-if="componente.icon">
						<use xlink:href="assets/icon/icon.svg#icon-phone">
						</use>
					</svg>
					<!-- end ngIf: componente.icon -->
					<h3 class="title-wrap-small" ng-bind-html="componente.name">
					Telefonia Fixa</h3>
				</span>
			</div>
			<div class="wrap-content-medium">
				<!-- ngIf: valor.offer && !$root.isMobile -->
				<div class="card-content telefonia" ng-class="{centralize:!card.showDetail}">
					<div class="wrap-info-card" mk-comp="molecule-simple-header-card" chave="telefonia_fixo_movel_ilimitado_local_empresas" parametros="{quantity: valor.quantity}">
						<!-- ngIf: componente.name -->
						<h5 class="plan-name" ng-class="{'plan-name':!componente.classFont}" ng-bind-html="componente.name" ng-if="componente.name">
						Ilimitado</h5>
						<!-- end ngIf: componente.name -->
						<!-- ngIf: componente.canais -->
						<!-- ngIf: componente.velocidade -->
						<!-- ngIf: componente.descriptionOld -->
						<!-- ngIf: componente.description -->
						<h4 class="plan-last-name" ng-if="componente.description">
							<strong>
							Local Empresas</strong>
						</h4>
						<!-- end ngIf: componente.description -->
						<!-- ngIf: componente.dependents -->
						<!-- ngIf: componente.wifi -->
						<div class="info-4p ng-hide" ng-show="componente.minutes || componente.movel_dependents" aria-hidden="true">
							<!-- ngIf: componente.minutesOld -->
							<span ng-bind-html="componente.minutes">
							</span>
							<span ng-bind-html="componente.movel_dependents">
							</span>
						</div>
						<!-- ngIf: componente.canaisHD -->
						<!-- ngIf: parametros.quantity -->
					</div>
					<!-- ngIf: valor.chave == 'tv' -->
					<!-- ngRepeat: adicional in valor.adicionais -->
					<!-- ngIf: (valor.offer || valor.offerExclusive) -->
					<div class="wrap-infos-card  show-itens" mk-comp="molecule-infos-card" chave="telefonia_fixo_movel_ilimitado_local_empresas" parametros="{sameHeight: false,hideTitle:true, cardType:'single-plans', show: card.showDetail, mkt_id: valor.mkt_id}">
						<!-- ngIf: componente.title && !parametros.hideTitle -->
						<!-- ngRepeat: (key, valor) in  componente[parametros.opcoes.selecionado] -->
						<!-- ngIf: componente.list && parametros.show -->
						<ul class="list list-check b2b color-gray-base-3 subtitle-5 border-bottom" list="componente.list" list-style="componente.listStyle" list-type="parametros.cardType" ng-if="componente.list &amp;&amp; parametros.show" parametros="{cardType:componente.list.listType}">
							<!-- ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							Fale ilimitado com fixos e celulares de qualquer operadora e em qualquer horário da sua cidade</li>
							<!-- end ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							Ligações para outro DDD e DDI serão cobradas à parte</li>
							<!-- end ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							1 linha de telefone fixo</li>
							<!-- end ngRepeat: item in list track by $index -->
							<li class="list-item" ng-repeat="item in list track by $index" ng-bind-html="item">
							Divulgação da sua empresa no site Apontador</li>
							<!-- end ngRepeat: item in list track by $index -->
						</ul>
						<!-- end ngIf: componente.list && parametros.show -->
						<!-- ngIf: componente.popover -->
						<!-- ngIf: parametros.card.labelOffer && !$root.isMobile -->
					</div>
				</div>
			</div>
		</div>
		<!-- end ngIf: valor.chave != 'adicionais' -->
		<!-- end ngRepeat: valor in arrDataComb -->
		<div class="card-footer">
			<!-- ngIf: !(horizontal || card.hideToggle) -->
			<div class="btn-show-infos" show="card.showDetail" title-atom="card.maisDetalhes" ng-if="!(horizontal || card.hideToggle)">
				<button class="btn-toggle active" ng-class="{active:show}" ng-click="show = !show" title="Mais informações do Combos 2 em 1, com 100 MEGA de internet, Fixo e Móvel Ilimitado Local Empresas" tabindex="0">
					<span class="left">
					</span>
					<span class="right">
					</span>
					<span class="left">
					</span>
					<span class="right">
					</span>
				</button>
			</div>
			<!-- end ngIf: !(horizontal || card.hideToggle) -->
			<div class="wrap-payment" card="card" title-atom="card.title" ng-class="card.classPrice">
				<div class="spinner ng-hide" ng-show="!price" aria-hidden="true"></div>
				<div class="wrapper-price molecule-price" ui-loading-dinamico="" ng-show="price" hide-button="card.hideButton" card="card" is-lazy-button="true" aria-hidden="false">
					<div class="wrap-value ">
						<!-- ngIf: boolAPartir && !hideAPartir && !product -->
						<!-- ngIf: boolAPartir && !hideAPartir && product -->
						<!-- ngIf: boolDePor && !boolAPartir -->
						<small class="wrap-small-per" ng-if="boolDePor &amp;&amp; !boolAPartir">
							De <i>
							R$ 174,89</i>
						por:</small>
						<!-- end ngIf: boolDePor && !boolAPartir -->
						<div class="price-content">
							<strong class="value-small">
							R$</strong>
							<strong class="value-large">
							144</strong>
							<div class="wrap-value-left">
								<strong class="value-small">
								,99</strong>
								<span class="value-extra-small" ng-hide="boolOnetime">
									/mês<span ng-class="{'hideAsteriscPrice':card.hideAsteriscPrice}">
									*</span>
								</span>
							</div>
						</div>
						<span ng-bind-html="card.labelPerLine" class="per-line">
						</span>
					</div>
					<!-- ngIf: button && !hideButton && forceRefreshButton -->
					<div class="wrap-btn-price" ng-if="button &amp;&amp; !hideButton &amp;&amp; forceRefreshButton">
						<button class=" btn-lg btn-orange" title="Assine já Combos 2 em 1, com 100 MEGA de internet, Fixo e Móvel Ilimitado Local Empresas" data-toggle="modal" data-target="#modal-mensagem">
							Assine já</button>
						
					</div>
					<!-- end ngIf: button && !hideButton && forceRefreshButton -->
				</div>
				<!-- ngIf: !card.staticFooterText && !card.price -->
				<p class="box-observation" ng-show="cycle" card="card" type="combo-oferta" template="combo" ng-if="!card.staticFooterText &amp;&amp; !card.price" aria-hidden="false">
					<span>
						Após o 12° ciclo, <strong>
						R$174,89</strong>
					por mês</span>
				</p>
				<!-- end ngIf: !card.staticFooterText && !card.price -->
				<!-- ngIf: card.staticFooterText -->
			</div>
			<!-- ngIf: card.legaltext -->
		</div>
		<!-- ngIf: horizontal && !$root.isMobile -->
	</div>
	<!-- ngIf: card.showComparator -->
	<!-- ngIf: card.showButtonInformation -->
</article>
</div>