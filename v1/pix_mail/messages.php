<?php
	$lang = array(
        // English is the default language, you can change the langugae from the config.php file
        'BR'=> array(
            'check_email'=>'Please enter a valid email!',
            'captcha'=>'Please check the Captcha!',
            'php_error'=>'Could not send mail! Please check your PHP mail configuration.',
            'success'=>'Obrigado! em breve, entraremos em contato com você.',
            'subscription'=>'Thank you for your Subscription.',
            'email_exists'=>'Email Already Exists',
        )
    );
?>